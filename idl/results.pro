pro results, psFile=psFile

  fPlotNum=0
  fPlotFails=0
  fPlotErrs=0
  fPlotTimes=1

  SColor=100
  EColor=60
  
  nLangTests=1972.
  nMathTests=2461.
  tLangAll=10.78
  tMathAll=33.92

  nprojects=10
  PNames=['BU','CE','CR', 'SU', 'Lang', $
          'Fld','FldE','Well','Math','Mean']

  xval=0.17
  yval=0.83
  yint=0.06
  
  SNum=[1800/nLangTests,5/nLangTests,1800/nLangTests,1800/nLangTests,1800/nLangTests, $
        2182/nMathTests,1./nMathTests,1308/nMathTests,2441/nMathTests]*100.
  ENum=[205/nLangTests,5/nLangTests,132/nLangTests,751/nLangTests, 470/nLangTests, $
        619/nMathTests,1/nMathTests,462/nMathTests,1984/nMathTests]*100.

  SFails=[2,0,3,2,0,0,7,1,0]
  EFails=[2,0,1,2,0,0,7,1,0]

  SErrs=[10,0,22,0,0,1,36,0,0]
  EErrs=[10,0,12,0,0,1,36,0,0]

  STimes=[10.2/tLangAll,3.7/tLangAll,11.2/tLangAll,11.2/tLangAll,14.2/tLangAll, $
          27.3/tMathAll,29.9/tMathAll,7.4/tMathAll,20.7/tMathAll]*100.
  ETimes=[2.6/tLangAll,2.7/tLangAll,2.7/tLangAll,9.3/tLangAll,5.8/tLangAll, $
          13.4/tMathAll,20.1/tMathAll,6.7/tMathAll,25.9/tMathAll]*100.

  if (fPlotNum eq 1) then begin
      time=findgen(nprojects)+1
      set_display,/tiny,psFile='numTests.eps'
      !p.multi=[0,1,1]
      !x.margin=[8,3]
      !y.margin=[2,2]
      plot,time,SNum,/nodata, $
           xrange=[0.5,nprojects+0.5],yrange=[0.,120.], $
           /xstyle,/ystyle, ytitle='% of All Tests', $
           xminor=1,yminor=1, $
           xticks=nprojects-1, xtickv=findgen(nprojects)+1, $
           xtickname=PNames,title='Number of Selected Tests'

      for i=0,nprojects-1 do begin
         if (i eq nprojects-1) then begin
           box,i+0.8,0,i+1.,mean(SNum),color=SColor ;30
           box,i+1.,0,i+1.2,mean(ENum),color=EColor ;210
        endif else begin
           box,i+0.8,0,i+1.,SNum[i],color=SColor ;30
           box,i+1.,0,i+1.2,ENum[i],color=EColor ;210
        endelse
      endfor

       xyouts,xval,yval,/normal,'STARTS',color=SColor
       xyouts,xval,yval-yint,/normal,'Ekstazi',color=EColor

       if (n_elements(psFile) gt 0) then begin
           device,/close
       endif
   endif

    if (fPlotFails eq 1) then begin
      time=findgen(nprojects)+1
      set_display,/tiny,psFile='numFails.eps'
      !p.multi=[0,1,1]
      !x.margin=[8,3]
      !y.margin=[2,2]
      plot,time,SNum,/nodata, $
           xrange=[0.5,nprojects+0.5],yrange=[0.,10.], $
           /xstyle,/ystyle, ytitle='#', $
           xminor=1,yminor=1, $
           xticks=nprojects-1, xtickv=findgen(nprojects)+1, $
           xtickname=PNames,title='Failures'

      for i=0,nprojects-1 do begin
         if (i eq nprojects-1) then begin
           box,i+0.8,0,i+1.,mean(SFails),color=SColor ;30
           box,i+1.,0,i+1.2,mean(EFails),color=EColor ;210
         endif else begin
           box,i+0.8,0,i+1.,SFails[i],color=SColor ;30
           box,i+1.,0,i+1.2,EFails[i],color=EColor ;210
         endelse
      endfor

       xyouts,xval,yval,/normal,'STARTS',color=SColor
       xyouts,xval,yval-yint,/normal,'Ekstazi',color=EColor

       if (n_elements(psFile) gt 0) then begin
           device,/close
       endif
  endif

  if (fPlotErrs eq 1) then begin
      time=findgen(nprojects)+1
      set_display,/tiny,psFile='numErrs.eps'
      !p.multi=[0,1,1]
      !x.margin=[8,3]
      !y.margin=[2,2]
      plot,time,SNum,/nodata, $
           xrange=[0.5,nprojects+0.5],yrange=[0.,40.], $
           /xstyle,/ystyle, ytitle='#', $
           xminor=1,yminor=1, $
           xticks=nprojects-1, xtickv=findgen(nprojects)+1, $
           xtickname=PNames,title='Errors'

      for i=0,nprojects-1 do begin
         if (i eq nprojects-1) then begin
           box,i+0.8,0,i+1.,mean(SErrs),color=SColor ;30
           box,i+1.,0,i+1.2,mean(EErrs),color=EColor ;210
         endif else begin
           box,i+0.8,0,i+1.,SErrs[i],color=SColor ;30
           box,i+1.,0,i+1.2,EErrs[i],color=EColor ;210
         endelse
      endfor

       xyouts,xval,yval,/normal,'STARTS',color=SColor
       xyouts,xval,yval-yint,/normal,'Ekstazi',color=EColor

       if (n_elements(psFile) gt 0) then begin
           device,/close
       endif
    endif

    if (fPlotTimes eq 1) then begin
      time=findgen(nprojects)+1
      set_display,/tiny,psFile='numTime.eps'
      !p.multi=[0,1,1]
      !x.margin=[8,3]
      !y.margin=[2,2]
      plot,time,SNum,/nodata, $
           xrange=[0.5,nprojects+0.5],yrange=[0.,150.], $
           /xstyle,/ystyle, ytitle='% Time of All Tests', $
           xminor=1,yminor=1, $
           xticks=nprojects-1, xtickv=findgen(nprojects)+1, $
           xtickname=PNames,title='Efficiency'

      for i=0,nprojects-1 do begin
         if (i eq nprojects-1) then begin
           box,i+0.8,0,i+1.,mean(STimes),color=SColor ;30
           box,i+1.,0,i+1.2,mean(ETimes),color=EColor ;210
        endif else begin
           box,i+0.8,0,i+1.,STimes[i],color=SColor ;30
           box,i+1.,0,i+1.2,ETimes[i],color=EColor ;210
         endelse
      endfor

       xyouts,xval,yval,/normal,'STARTS',color=SColor
       xyouts,xval,yval-yint,/normal,'Ekstazi',color=EColor

       if (n_elements(psFile) gt 0) then begin
           device,/close
       endif
  endif
  
end

