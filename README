Comparing Static, Dynamic, and Machine Learning
Regression Test Selection Techniques
================================================

A poster for this project is TestSelectionPoster.pdf.
A report for this project is TestSelectionReport.pdf.


For this project, I compared three different regression testing tools:
- CUTER: ClUstering-based TEst suite Reduction by Carmen Coviello,
  Simone Romano and Giuseppe Scanniello at the University of Basilicata.
  * Download from http://www2.unibas.it/sromano/CUTER.html

- Ekstazi by Milos Gligoric, Lamyaa Eloussi and Darko Marinov at the
  University of Illinois at Urbana-Champaign.
  * Available at http://ekstazi.org/index.html

- STARTS: STAtic Regression Test Selection by Owolabi Legunsen, August Shi,
  and Darko Marinov at the University of Illinois at Urbana-Champaign.
  * Available at https://github.com/TestingResearchIllinois/starts


I evaluated these three tools on three open source libraries:
- Apache Commons Lang versions 2.6 and 3.9
  * https://commons.apache.org/proper/commons-lang/index.html

- Apache Commons Math versions 2.2 and 3.6
  * https://commons.apache.org/proper/commons-math/

- JTopas version 0.8
  * https://sourceforge.net/projects/jtopas/


Analysis of the results was performed using IDL, using the program
in the IDL directory.


----------
Abstract
----------
Regression testing is essential in order to ensure that software changes do not
harm any existing behaviors; however, it becomes very costly to continue to
execute all existing test cases as software matures.  A variety of different
approaches have been developed in the literature, with work focusing on three
main branches: test suite reduction, test case selection, and test case
prioritization.  This project has two main goals: 1) to identify recent
software techniques for regression testing in each of the branches, and 2) to
compare and evaluate three tools that use different approaches (CUTER, Ekstazi,
and STARTS).  Using results from this project and obtained in recent related
literature, Ekstazi seems to be the automated test-selection tool best suited
for integration into various developer programming environments because of its
efficiency, safety, precision, support, and usability.  This project highlights
difficulties in regression testing research due to both developing techniques
that satisfy all desirable requirements and to creating a tool that can be used
widely by the community.


--------------------------------
Katherine Haynes
Colorado State University
Department of Computer Science
CS 514; December 2019